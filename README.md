# CBAS_Pictures

## A centralized repo for CBAS images used in different projects.

**Note** about the micro photos. These photos were taken using different objectives, which are color labeled. The scale for the different photos was then done using the photos of the Neubauer chamber taken under the same optical conditions as the photos.

Macro photos ussually include an scale bar.

This repository will start to use git-lfs to avoid making its size grow too large. Possibly in the future the images will be migrated to another server and only low resolution images will be provided here, together with links to the high-resolutions files.

The names of the folders in this repository should be self explanatory, and inside each folder I try to keep a README.md file with further information on the photos, etc.


#### NOTES

1. This repository (or parts of it) is in active development. Check the releases section to see if there are snap-shots of it.

2. This repository is **public** and the following personal information is visible:
	* After each **commit*, the name and E-mail address as saved in your Git config.
	* The **GitLab username* as saved in your user settings (under Account).

3. In German: Bitte beachten Sie: Bei öffentlichen Projekten sind die folgenden Informationen öffentlich sichtbar (allgemein zugreifbar):
	* Bei jedem Commit der Name und die E-Mail-Adresse, wie sie in der Konfiguration von Git hinterlegt wurden.
	* Bei GitLab der GitLab-Benutzername (Username), wie er bei den Benutzereinstellungen (User Settings) im Abschnitt "Konto" (Account) festgelegt wird. Der Benutzername ist bei persönlichen Projekten im Pfad sichtbar.

#### Note that by commiting to this repository you accept the publication of the information detailed above. More information about public repositories can be find here: https://doku.lrz.de/display/PUBLIC/GitLab


Sergio


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
